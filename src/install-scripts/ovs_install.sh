#!/bin/bash -x
OVSDIR=openvswitch-{$1}

#prerequisites
apt-get update
apt-get install -y git python-simplejson python-qt4 python-twisted-conch automake autoconf gcc uml-utilities libtool build-essential git pkg-config linux-headers-`uname -r`

#download the source
if [ ! -d "$OVSDIR" ]; then
    wget http://openvswitch.org/releases/openvswitch-2.3.0.tar.gz
    tar -xzvf openvswitch-2.3.0.tar.gz
    rm -rf openvswitch-2.3.0.tar.gz
    mv openvswitch-2.3.0 $OVSDIR
fi
cd $OVSDIR

#install
./boot.sh
./configure --with-linux=/lib/modules/`uname -r`/build
make && make install && make modules_install

#setup modules
 modprobe openvswitch
 lsmod | grep openvswitch
touch /usr/local/etc/ovs-vswitchd.conf
 mkdir -p /usr/local/etc/openvswitch

#setup schema
ovsdb-tool create /usr/local/etc/openvswitch/conf.db  vswitchd/vswitch.ovsschema

#start the server
ovsdb-server /usr/local/etc/openvswitch/conf.db \
    --remote=punix:/usr/local/var/run/openvswitch/db.sock \
    --remote=db:Open_vSwitch,Open_vSwitch,manager_options \
    --bootstrap-ca-cert=db:Open_vSwitch,SSL,ca_cert --pidfile --detach --log-file

sleep 2

ovs-vsctl --no-wait init
ovs-vswitchd --pidfile --detach
ovs-vsctl show

#verify process
ps -ea | grep ovs

cd ../
rm -rf $OVSDIR
