#!/bin/sh
cd ~
sudo apt-get update
sudo apt-get install -y python-setuptools
sudo apt-get install -y xauth xorg openbox
sudo apt-get install -y python-termcolor
sudo apt-get install -y screen vim
sudo apt-get install -y r-base
git clone https://github.com/vikasuy/cs244-proj3.git
cd cs244-proj3
sudo python setup.py
sudo python gen_table.py
cp ~/cs693-final-project/src/tcpfastopen/gen_table.py .
cp ~/cs693-final-project/src/tcpfastopen/runall.sh .
cp ~/cs693-final-project/src/tcpfastopen/ens_final.sh .
sudo ./runall.sh
