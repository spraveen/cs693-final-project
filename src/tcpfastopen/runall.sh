#!/bin/bash -x
x=1
limit=40
mkdir -p results/
while [ $x -le $limit ]
do
    echo "Welcome $x times"
    sudo python gen_table.py
    cp table.txt results/table[$x].txt
    x=$(( $x + 1 ))
done
cd results
sudo grep html table* | sed 's/ //g' | sed 's/|/,/g' | cut -d',' -f2- > ../readings.csv
cd ../
sudo ./ens_final.sh
