#!/usr/bin/python

"""
CS244 Project 3
TFO Profiling
"""

from mininet.topo import Topo
from mininet.node import CPULimitedHost, OVSController
from mininet.link import TCLink
from mininet.net import Mininet
from mininet.log import lg, info
from mininet.util import dumpNodeConnections
from mininet.cli import CLI

from subprocess import Popen, PIPE
from time import sleep, time
from multiprocessing import Process
from argparse import ArgumentParser

import sys
import os
import math

TCP_DUMP_FILE = "tcp_dump.out"
MGET_BINARY = "mget"

class TFOTopo(Topo):
    "Simple topology for TFO profiling"

    def __init__(self, delay):
        super(TFOTopo, self).__init__()

        # create two hosts 
        h1 = self.addHost('h1')
        h2 = self.addHost('h2')

        # Here I have created a switch.  If you change its name, its
        # interface names will change from s0-eth1 to newname-eth1.
        switch = self.addSwitch('s0')

        # Add links with appropriate characteristics
        delay_per_link = int(delay / 4)
        delay_string = '%sms' % delay_per_link
        self.addLink(h1, switch, delay=delay_string)
        self.addLink(h2, switch, delay=delay_string)
        return

def start_tcpdump(net):
    print "Starting tcp dump..."
    h1 = net.getNodeByName('h1')
    proc = h1.popen("tcpdump -w foo.out", shell=True)
    sleep(1)
    return [proc]

def get_tcpdump(net):
    h1 = net.getNodeByName('h1')
    output = h1.cmd("tcpdump -r foo.out", shell=True)
    with open(TCP_DUMP_FILE, 'w') as f:
        f.write(output)

def start_webserver(net):
    print "Starting web server..."
    h2 = net.getNodeByName('h2')
    proc = h2.popen("sudo python server/tfo-server.py", shell=True)
    sleep(1)
    return [proc]

def mean(l):
    return float(sum(l)) / len(l)

NUM_TRIALS = 1
def time_webrequests(net, mget_cmd, site):
    h1 = net.getNodeByName('h1')
    h2 = net.getNodeByName('h2')
    server_url = "%s/%s" % (h2.IP(), site)

    times = []
    base_cmd = '/usr/bin/time -f "%e"'
    full_mget_cmd = '%s %s -r -p --no-http-keep-alive --no-cache --delete-after --directories=off %s > %s' % (base_cmd, mget_cmd, server_url, '/dev/null' )

 
    print full_mget_cmd
    for i in xrange(NUM_TRIALS): 
        p = h1.popen(full_mget_cmd)
        _, response_time = p.communicate()
        time_start_index = response_time.find('"') + 1
        time_end_index = response_time.find('"', time_start_index)
        response_time = float(response_time[time_start_index : time_end_index]) 
        times.append(response_time) 
        print "request took %0.2f seconds" % response_time

    return mean(times)

def do_webrequests(net, site):
    tfo_time = time_webrequests(net, 'mget-tfo', site)
    return tfo_time

def tfo_test(delay, site, tcp_dump=False):
    topo = TFOTopo(delay)
    net = Mininet(topo=topo, host=CPULimitedHost, link=TCLink, controller=OVSController)
    net.start()

    # This dumps the topology and how nodes are interconnected through
    # links.

    # dumpNodeConnections(net.hosts)
    # This performs a basic all pairs ping test.
    # net.pingAll()

    if tcp_dump:
        start_tcpdump(net)
    start_webserver(net)

    time = time_webrequests(net, MGET_BINARY, site)

    if tcp_dump:
        get_tcpdump(net)
    
    # Hint: The command below invokes a CLI which you can use to
    # debug.  It allows you to run arbitrary commands inside your
    # emulated hosts h1 and h2.
    # CLI(net)

    net.stop()

    # Ensure that all processes you create within Mininet are killed.
    # Sometimes they require manual killing.
    Popen("pgrep -f tfo-server.py | xargs kill -9", shell=True).wait()
    if tcp_dump:
        Popen("pgrep -f tcpdump | xargs kill -9", shell=True).wait()

    return time

if __name__ == "__main__":
    parser = ArgumentParser(description="TFO Profiling")
    parser.add_argument('--delay',
                        type=float,
                        help="Link propagation delay (ms)",
                        required=True)
    parser.add_argument('--site',
                        help="Which site to download",
                        required=True)
    parser.add_argument("--tcp_dump",
                        help="Whether to run tcp dump or not",
                        required=False)
    args = parser.parse_args()

    print tfo_test(args.delay, args.site, args.tcp_dump)
