#!/usr/bin/python
"""
Gets timings of tfo/non-tfo on various sites and creates table of results. 
must be run with sudo privilege
"""

import os
from tfo_test import tfo_test
from prettytable import PrettyTable
import subprocess
from argparse import ArgumentParser
import random

"""
enable tfo (but bypass cookie requirements, so that the first request uses
tfo. 519 is an AND of 0x200, 1, 2, and 4 
see link below and scroll down to the tcp_fastopen part
http://www.mjmwired.net/kernel/Documentation/networking/ip-sysctl.txt
"""
def enable_tfo():
    set_tfo_flag(519)

def disable_tfo():
    set_tfo_flag(0)

def set_tfo_flag(val):
    with open('/dev/null', 'w') as devnull:
        subprocess.call(
            'echo %d | sudo tee /proc/sys/net/ipv4/tcp_fastopen' % val, 
            shell=True, 
            stdout=devnull
        )

""" sets value in the table based on the given timings """
def update_table(table, timings_tfo, timings_no_tfo):
    sites = timings_tfo.keys()
    sites.sort()
    rtts = timings_tfo[sites[0]].keys()
    rtts.sort()
    for site in sites:
        for rtt in rtts:
            no_tfo_time = timings_no_tfo[site][rtt]
            tfo_time = timings_tfo[site][rtt]
            improvement = round(((no_tfo_time - tfo_time) * 100) / no_tfo_time)
            site_to_print = os.path.basename(site) #if rtt is 20 else ''
            table.add_row(
                [site_to_print, rtt, no_tfo_time, tfo_time, '%d%%' % improvement]
            )
 
""" Gets times across all sites and all RTTs """
def get_results(alexa):
    timings = {}
    sites_dir = 'server/sites' if not alexa else 'server/alexa'
    sites = os.listdir(sites_dir)
    for site in sites:
        if not site.endswith('.html'):
            continue
        site = os.path.join(sites_dir, site)
        timings[site] = {}
        for rtt in [20, 100, 200]:
            print "\nTiming for site %s and rtt %d" % (site, rtt)
            results = tfo_test(delay=rtt, site=site)
            timings[site][rtt] = results

    return timings


def gen_table():
    parser = ArgumentParser(description="Table Generation")
    parser.add_argument('--alexa',
                        action='store_true',
                        help="use alexa sites",
                        default=False)

    parser.add_argument('--output_dir',
                        help="Where to print table",
                        default='.')

    args = parser.parse_args()

    subprocess.call(['mn', '-c'])
    table = PrettyTable(
        ['Page', 'RTT(ms)', 'PLT : non-TFO (s)', 'PLT : TFO (s)', 'Improvement']
    )

    enable_tfo()
    print "\nRunning with TFO Enabled"
    timings_tfo = get_results(args.alexa)

    disable_tfo()
    print "\nRunning with TFO Disabled"
    timings_no_tfo = get_results(args.alexa)
 
    update_table(table, timings_tfo, timings_no_tfo)
    output_file = 'table-alexa.txt' if args.alexa else 'table.txt'
    with open(os.path.join(args.output_dir, output_file), 'w') as f:
        f.write(table.get_string())
 
    print table

if __name__ == '__main__':
    gen_table()
