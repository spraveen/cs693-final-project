#!/usr/bin/Rscript

table <-read.csv(file="readings.csv", header=FALSE, sep=",")

rtt20ntfo <-subset(table$V3, table$V2==20)
rtt20tfo <-subset(table$V4, table$V2==20)


rtt100ntfo <-subset(table$V3, table$V2==100)
rtt100tfo <-subset(table$V4, table$V2==100)

rtt200ntfo <-subset(table$V3, table$V2==200)
rtt200tfo <-subset(table$V4, table$V2==200)

x<-c(20, 20, 100, 100, 200, 200)
y<-c(mean(rtt20ntfo), mean(rtt20tfo), mean(rtt100ntfo), mean(rtt100tfo), mean(rtt200ntfo), mean(rtt200tfo))

yHigh20ntfo <- mean(rtt20ntfo) + qnorm(0.975)*sd(rtt20ntfo)/sqrt(40)
yLow20ntfo<- mean(rtt20ntfo) - qnorm(0.975)*sd(rtt20ntfo)/sqrt(40)

yHigh20tfo <- mean(rtt20tfo) + qnorm(0.975)*sd(rtt20tfo)/sqrt(40)
yLow20tfo<- mean(rtt20tfo) - qnorm(0.975)*sd(rtt20tfo)/sqrt(40)

yHigh100ntfo <- mean(rtt100ntfo) + qnorm(0.975)*sd(rtt100ntfo)/sqrt(40)
yLow100ntfo<- mean(rtt100ntfo) - qnorm(0.975)*sd(rtt100ntfo)/sqrt(40)

yHigh100tfo <- mean(rtt100tfo) + qnorm(0.975)*sd(rtt100tfo)/sqrt(40)
yLow100tfo<- mean(rtt100tfo) - qnorm(0.975)*sd(rtt100tfo)/sqrt(40)

yHigh200ntfo <- mean(rtt200ntfo) + qnorm(0.975)*sd(rtt200ntfo)/sqrt(40)
yLow200ntfo<- mean(rtt200ntfo) - qnorm(0.975)*sd(rtt200ntfo)/sqrt(40)

yHigh200tfo <- mean(rtt200tfo) + qnorm(0.975)*sd(rtt200tfo)/sqrt(40)
yLow200tfo<- mean(rtt200tfo) - qnorm(0.975)*sd(rtt200tfo)/sqrt(40)

plot(x,y,xlab="RTT",ylab="Response",main="Comparison Plot of Average Page Load Time", ylim=c(0,25))
legend(150,25, c("non-TFO","TFO"), lty=c(1,1), lwd=c(2.5,2.5), col=c(4,6))

xHigh <-20
xLow <-20
print("20 RTT NTFO Mean (low,high)")
x<-mean(rtt20ntfo)
print(x)
print(yLow20ntfo)
print(yHigh20ntfo)
arrows(xHigh,yHigh20ntfo,xLow,yLow20ntfo,col=4,angle=90,length=0.1,code=3)

print("20 RTT TFO Mean (low,high)")
y<-mean(rtt20tfo)
print(y)
print(yLow20tfo)
print(yHigh20tfo)
arrows(xHigh,yHigh20tfo,xLow,yLow20tfo,col=6,angle=90,length=0.1,code=3)
z=x-y
per=(z/x)*100
print("Improvement%")
print(per)
imp = signif(per, digits = 4)
text(xHigh,yHigh20ntfo, imp, pos=3)
print("===========================================")

xHigh <-100
xLow <-100
print("100 RTT NTFO Mean (low,high)")
x<-mean(rtt100ntfo)
print(x)
print(yLow100ntfo)
print(yHigh100ntfo)
arrows(xHigh,yHigh100ntfo,xLow,yLow100ntfo,col=4,angle=90,length=0.1,code=3)

print("100 RTT TFO Mean (low,high)")
y<-mean(rtt100tfo)
print(y)
print(yLow100tfo)
print(yHigh100tfo)
arrows(xHigh,yHigh100tfo,xLow,yLow100tfo,col=6,angle=90,length=0.1,code=3)
z=x-y
per=(z/x)*100
print("Improvement%")
print(per)
imp = signif(per, digits = 4)
text(xHigh,yHigh100ntfo, imp, pos=3)
print("===========================================")

xHigh <-200
xLow <-200
print("200 RTT NTFO Mean (low,high)")
x<-mean(rtt200ntfo)
print(x)
print(yLow200ntfo)
print(yHigh200ntfo)
arrows(xHigh,yHigh200ntfo,xLow,yLow200ntfo,col=4,angle=90,length=0.1,code=3)

print("200 RTT TFO Mean (low,high)")
y<-mean(rtt200tfo)
print(y)
print(yLow200tfo)
print(yHigh200tfo)
arrows(xHigh,yHigh200tfo,xLow,yLow200tfo,col=6,angle=90,length=0.1,code=3)
z=x-y
per=(z/x)*100
print("Improvement%")
print(per)
imp = signif(per, digits = 4)
text(xHigh,yHigh200ntfo, imp, pos=3)
print("===========================================")

